const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv')

const app = express()

dotenv.config()

const PORT = process.env.PORT

// Connect database
mongoose.connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
.then(() => {
    console.log('MongoDB is connected')
})
.catch((err) => {
    console.log(err)
})

// Middleware
app.use(express.json())

// Routes
app.use('/user', require('./routes/user'))

app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`)
})