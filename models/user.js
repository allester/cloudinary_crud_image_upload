const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    name: {
        type: String
    },
    avatar: {
        type: String
    },
    cloudinary_id: {
        type: String
    },
    image_format: {
        type: String
    },
    image_url: {
        type: String
    },
    image_secure_url: {
        type: String
    }
})

module.exports = mongoose.model("User", userSchema)