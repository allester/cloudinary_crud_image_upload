const express = require('express')
const router = express.Router()
const cloudinary = require('../utils/cloudinary')
const upload = require('../utils/multer')

const User = require('../models/user')

router.post('/', upload.single('image'), async (req, res) => {
    try {
        
        const result = await cloudinary.uploader.upload(req.file.path)

        // Create instance of user
        let user = new User({
            name: req.body.name,
            avatar: result.secure_url,
            cloudinary_id: result.public_id,
            image_format: result.format,
            image_url: result.url,
            image_secure_url: result.secure_url
        })

        // Save user
        await user.save()

        res.json(user)

    } catch (error) {

        console.log(error)

    }
})

router.get('/', async (req, res) => {

    try {
        let user = await User.find()

        res.json(user)
    } catch (error) {
        console.log(error)
    }

})

router.put('/:id', upload.single('image'), async (req, res) => {

    try {
        
        let user = await User.findById(req.params.id)

        await cloudinary.uploader.destroy(user.cloudinary_id)

        const result = await cloudinary.uploader.upload(req.file.path)

        const data = {
            name: req.body.name || user.name,
            avatar: result.secure_url || user.avatar,
            cloudinary_id: result.public_id || user.cloudinary_id,
            image_format: result.image_format || user.image_format,
            image_url: result.image_url || user.image_url,
            image_secure_url: result.secure_url || user.image_secure_url
        }

        user = await User.findByIdAndUpdate(req.params.id, data, { new: true })

        res.json(user)

    } catch (error) {
        console.log(error)
    }

})

router.delete('/avatar/:id', async (req, res) => {

    try {
        
        // Find user by id
        let user = await User.findById(req.params.id)

        // Delete image from cloudinary
        await cloudinary.uploader.destroy(user.cloudinary_id)

        // Detach image from user

        user.avatar = null
        user.cloudinary_id = null
        user.image_format = null
        user.image_url = null
        user.image_secure_url = null

        let result = await user.save()

        res.json(result)

    } catch (error) {
        
        console.log(error)

    }

})

module.exports = router