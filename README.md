# Cloudinary Image Upload - CRUD Operations

Upload images to cloudinary and perform Create, Read, Update and Delete operations on the same image.

This repository assumes you're using the MongoDB database by default.

Clone and reconfigure to use any other database service according to your preference.

## Instructions

Clone repository to your local environment

Create a .env file in the root of the directory

Add the following environment variables

+ PORT
+ MONGO_URI
+ CLOUDINARY_CLOUD_NAME
+ CLOUDINARY_API_KEY
+ CLOUDINARY_API_SECRET

Run the following command to start the server

### Install Packages
    npm install

### Dev Server
    npm run dev

### Production Server
    npm run start